<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>时间表达式编辑器</title>
<%-- <%@include file="/views/common/common.jsp"%> --%>

<link href="<%=request.getContextPath()%>/views/system/zjob/cron-generator/bootstrap.min.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/views/system/zjob/cron-generator/font/font-awesome.min.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/views/system/zjob/cron-generator/cronGen.css" rel="stylesheet">
<script src="<%=request.getContextPath()%>/views/system/zjob/cron-generator/jquery-2.1.4.min.js"></script>
<script src="<%=request.getContextPath()%>/views/system/zjob/cron-generator/cronGen.js"></script>
<script src="<%=request.getContextPath()%>/views/system/zjob/cron-generator/bootstrap.min.js"></script>

<script type="text/javascript">

$(function() {
    $("#cron").cronGen({
    	direction : 'right'
    });
	$('#setbutton').trigger("click");
});

function closePage(){
	var cronvalue = $('#cron').val();
	 window.opener.OpenPageReturnValue('jobtime_id',cronvalue);
	 window.close();
}
</script>


</head>
<body>
	<div class="container-fluid">
		<div class="row-fluid">
			<form role="form" class="form-inline">
				<div class="form-group">
					<input id="cron" class="form-control" />
				</div>
			</form>
		</div>
		
		<div class="row-fluid">
			<button class="btn btn-primary btn-lg fixed-bottom" onclick="closePage()">保存表达式</button>
		</div>
	</div>
</body>
</html>