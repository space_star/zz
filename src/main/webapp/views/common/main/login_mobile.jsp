<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>${sp.system_title}</title>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">
$(document).ready(function(){ 
	$("#userid").focus();
}); 
function LoginSystem(){
	var userid = $("#userid").val();
	var passwordid = $("#passwordid").val();
	if(!isNull(userid)){
		if(!isNull(passwordid)){
				//判读账号对应分配组织是否这多个
				$.ajax({
					type : "GET",
					url : "getUserOrgForUserId",
					data:{userId:userid},
					success : function(data) {
						if(data.code=="SUCCESS"){
							var orgCount = data.data.length;
							if(orgCount==1){
								$("#orgid_id").val(data.data[0].zid);
								$("#LoginForm").submit();
							}else if(orgCount>1){
								$('#MainDiv').empty();
								//设置多组织选择
								var orgListHtml = '<div class="col-md-12"><ul>';
								orgListHtml = orgListHtml+'<form id="LoginForm" method="get" action="UserLogin" enctype="multipart/form-data">';
								orgListHtml = orgListHtml+'<input type="hidden" id="orgid_id" name="orgid"/>';
								orgListHtml = orgListHtml+'<input type="hidden" name="user_id" value=\''+userid+'\'/>';
								orgListHtml = orgListHtml+'<input type="hidden" name="password" value=\''+passwordid+'\'/>'; 
								orgListHtml = orgListHtml+'</form>';
								for(var i=0;i<orgCount;i++){
									orgListHtml = orgListHtml+'<li style="background:url(\'img/system/login_jiantou0.png\') no-repeat left 7px;" onmouseout="li_onmouseout(this)" onmouseover="li_onmouseover(this)"><a href="javascript:void(0);" class="text3" onclick="SetOrgid(\''+data.data[i].zid+'\')" >'+data.data[i].full_org_name+'</a></li>';
								}
								orgListHtml = orgListHtml+'</ul></div>';
								$('#MainDiv').append(orgListHtml);
								
								//设置返回登录页面按钮
								$('#ButtonDiv').empty();
								var ReturnLoginPageHtml = '<div class="col-md-12"><a href="login" class="text3" style="padding-left: 21px">返回登录</a></div>';
								$('#ButtonDiv').append(ReturnLoginPageHtml);
								
							}else if(orgCount==0){
								if(userid=='sa'){
									$("#LoginForm").submit();
								}else{
									$("#UserLoginAlert").html(data.msg);
								}
								
							}
						}
					}
				});
		}else{
			$("#UserLoginAlert").html("密码不能为空");
			document.getElementById('passwordid').focus(); 
		}
	}else{
		$("#UserLoginAlert").html("账号不能为空");
		document.getElementById('userid').focus(); 
	}
	
	
}

function SetOrgid(orgid){
	$("#orgid_id").val(orgid);
	$("#LoginForm").submit();
}

function Login_onkeydown(){
	if(event.keyCode==13) {
		var userid = $("#userid").val();
		var passwordid = $("#passwordid").val();
		if (isNull(userid)){ 
			$("#userid").focus();
		    return; 
		}else if (isNull(passwordid)){ 
			$("#passwordid").focus();
		    return; 
		}else{
			LoginSystem(); 
		}
	}
	
}

/**
 * 多组织选择.li鼠标移入
 * @param obj
 */
function li_onmouseover(obj){
    obj.style.background = "url('img/system/login_jiantou1.png') no-repeat left 7px";
}

/**
 * 多组织选择.li鼠标移出
 * @param obj
 */
function li_onmouseout(obj){
    obj.style.background = "url('img/system/login_jiantou0.png') no-repeat left 7px";
}
</script>
<style type="text/css">

 /*清除默认样式*/


ul {
	padding-left: 11px;
}

li {
	list-style: none;
	padding-left: 30px;
	background: url('img/system/login_jiantou2.png') no-repeat left 7px;
	margin-bottom: 20px;
	margin-left: 0px;
}


.text2 {
	color: #fff;
	font-size: 20px;
	font-family: 微软雅黑;
	font-weight: 500;
}

.text3 {
	color: #BAB9B8;
	font-size: 20px;
	font-family: 微软雅黑;
}

</style>
</head>
<body>
<div class="container">
			<div class="row" style="padding-top: 10%;padding-bottom:10%; background-color: #213477;">
				<div class="col-md-12">
					<samp class="text2">欢迎登录 | ${sp.system_title}</samp>
				</div>
				<div class="col-md-12">
					<samp class="text2">${sp.login_notice}</samp>
				</div>
			</div>
			<div id="MainDiv" class="row" style="padding-top: 20%;">
				<div class="col-md-12">
					<form action="UserLogin" method="get" id="LoginForm" enctype="multipart/form-data">
						<input type="hidden" id="orgid_id" name="orgid" />
						<div class="col-md-12">
							<samp style="color: #6d6c67; font-size: 20px; font-family: 微软雅黑;">账 号</samp>
						</div>
						<div class="col-md-12 pt-2">
							<input class="form-control form-control-lg" type="text" id="userid" name="user_id" value="${user_id}" placeholder="用户名/手机" onkeydown="Login_onkeydown();" maxlength="16" autofocus="autofocus" style="font-size: 20px; outline: none;" />
						</div>
						<div class="col-md-12 pt-4"> 
							<samp style="color: #6d6c67; font-size: 20px; font-family: 微软雅黑;">密 码</samp>
						</div>
						<div class="col-md-12 pt-2">
							<input class="form-control form-control-lg" type="password" id="passwordid" name="password" placeholder="********" onkeydown="Login_onkeydown();" style=" font-size: 20px; outline: none;" /> 
						</div>
					</form>
				</div>
				<div class="col-md-12 pt-5 d-flex justify-content-center" >
					<button type="button" onclick="LoginSystem()" class="btn btn-secondary btn-lg " style="background-color: #213477; color: white;width: 80%;">登录</button>
				</div>
			</div>
			
			<div id="ButtonDiv" class="row" style="padding-top: 20px; padding-left: 6%;">
				<div class="col-md-12">
					<label id="UserLoginAlert" style="color: red;">${UserLoginAlert}</label>
				</div>
			</div>
		
		<div class="row fixed-bottom m-2"> 
			©2018-2020 <a href="https://www.zframeworks.com/" class="z_blue" target="_blank"> zframeworks.com </a> 版权所有
				<a href="http://beian.miit.gov.cn/publish/query/indexFirst.action" class="z_blue" target="_blank">辽ICP备14004252号</a>
				 | 系统版本号：<a href="https://blog.csdn.net/qq_38056435/article/details/70212001" class="z_blue" target="_blank">${sp.zversion}</a>
		</div>
</div>
</body>
</html>