<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/views/common/main/main_head.jsp"%>	
</head>
<body>
<div class="container">
	<!-- 目录 -->
	<div class="row" style="padding: 10px;margin:0px;overflow-x:hidden;">
  		<div id="SideMenuTree" class="easyui-sidemenu" data-options="data:menuTreeList,border:false,onSelect:MenuTreeSelectMobile,multiple:false" style="width: 100%" ></div>
  	</div>
  	
  	
	<!-- 底部导航 -->
	<%@include file="/views/common/main/foot_mobile.jsp"%>
</div>
</body>
</html>