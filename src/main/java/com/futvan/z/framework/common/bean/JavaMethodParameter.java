package com.futvan.z.framework.common.bean;
/**
 * JAVA方法参数对象
 * @author zz
 *
 */
public class JavaMethodParameter {
	private String type;//类型 String,int ....
	private String name;//名称 
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
