package com.futvan.z.system.zdb;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_db_table extends SuperBean{
	//表ID
	private String table_id;

	//表名称
	private String table_title;

	//主键字段
	private String table_pk;

	//字段信息
	private List<z_db_table_column> z_db_table_column_list;

	/**
	* get表ID
	* @return table_id
	*/
	public String getTable_id() {
		return table_id;
  	}

	/**
	* set表ID
	* @return table_id
	*/
	public void setTable_id(String table_id) {
		this.table_id = table_id;
 	}

	/**
	* get表名称
	* @return table_title
	*/
	public String getTable_title() {
		return table_title;
  	}

	/**
	* set表名称
	* @return table_title
	*/
	public void setTable_title(String table_title) {
		this.table_title = table_title;
 	}

	/**
	* get主键字段
	* @return table_pk
	*/
	public String getTable_pk() {
		return table_pk;
  	}

	/**
	* set主键字段
	* @return table_pk
	*/
	public void setTable_pk(String table_pk) {
		this.table_pk = table_pk;
 	}

	/**
	* get字段信息
	* @return 字段信息
	*/
	public List<z_db_table_column> getZ_db_table_column_list() {
		return z_db_table_column_list;
  	}

	/**
	* set字段信息
	* @return 字段信息
	*/
	public void setZ_db_table_column_list(List<z_db_table_column> z_db_table_column_list) {
		this.z_db_table_column_list = z_db_table_column_list;
 	}

}
