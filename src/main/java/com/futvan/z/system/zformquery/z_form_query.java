package com.futvan.z.system.zformquery;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_form_query extends SuperBean{
	//用户
	private String userid;

	//关联表
	private String tableid;

	//查询条件
	private String queryinfo;

	/**
	* get用户
	* @return userid
	*/
	public String getUserid() {
		return userid;
  	}

	/**
	* set用户
	* @return userid
	*/
	public void setUserid(String userid) {
		this.userid = userid;
 	}

	/**
	* get关联表
	* @return tableid
	*/
	public String getTableid() {
		return tableid;
  	}

	/**
	* set关联表
	* @return tableid
	*/
	public void setTableid(String tableid) {
		this.tableid = tableid;
 	}

	/**
	* get查询条件
	* @return queryinfo
	*/
	public String getQueryinfo() {
		return queryinfo;
  	}

	/**
	* set查询条件
	* @return queryinfo
	*/
	public void setQueryinfo(String queryinfo) {
		this.queryinfo = queryinfo;
 	}

}
