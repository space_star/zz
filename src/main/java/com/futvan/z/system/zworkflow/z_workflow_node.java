package com.futvan.z.system.zworkflow;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_workflow_node extends SuperBean{
	//节点标题
	private String node_title;

	//节点类型
	private String node_type;

	//处理人类型
	private String workflow_user_type;

	//处理人
	private String node_user;

	//审批人为空时
	private String approverisnull;

	//处理角色
	private String node_role;

	//审批方式
	private String processing_mode;

	//多级审批层数
	private String approvals_up_num;

	//多级审批终点角色
	private String end_approval_role;

	//表单部门字段
	private String form_org;

	//处理方法
	private String node_action;

	//审批通过下一节点
	private String node_true;

	//审批未通过下一节点
	private String node_false;

	//节点分支条件
	private List<z_workflow_node_branch> z_workflow_node_branch_list;

	/**
	* get节点标题
	* @return node_title
	*/
	public String getNode_title() {
		return node_title;
  	}

	/**
	* set节点标题
	* @return node_title
	*/
	public void setNode_title(String node_title) {
		this.node_title = node_title;
 	}

	/**
	* get节点类型
	* @return node_type
	*/
	public String getNode_type() {
		return node_type;
  	}

	/**
	* set节点类型
	* @return node_type
	*/
	public void setNode_type(String node_type) {
		this.node_type = node_type;
 	}

	/**
	* get处理人类型
	* @return workflow_user_type
	*/
	public String getWorkflow_user_type() {
		return workflow_user_type;
  	}

	/**
	* set处理人类型
	* @return workflow_user_type
	*/
	public void setWorkflow_user_type(String workflow_user_type) {
		this.workflow_user_type = workflow_user_type;
 	}

	/**
	* get处理人
	* @return node_user
	*/
	public String getNode_user() {
		return node_user;
  	}

	/**
	* set处理人
	* @return node_user
	*/
	public void setNode_user(String node_user) {
		this.node_user = node_user;
 	}

	/**
	* get审批人为空时
	* @return approverisnull
	*/
	public String getApproverisnull() {
		return approverisnull;
  	}

	/**
	* set审批人为空时
	* @return approverisnull
	*/
	public void setApproverisnull(String approverisnull) {
		this.approverisnull = approverisnull;
 	}

	/**
	* get处理角色
	* @return node_role
	*/
	public String getNode_role() {
		return node_role;
  	}

	/**
	* set处理角色
	* @return node_role
	*/
	public void setNode_role(String node_role) {
		this.node_role = node_role;
 	}

	/**
	* get审批方式
	* @return processing_mode
	*/
	public String getProcessing_mode() {
		return processing_mode;
  	}

	/**
	* set审批方式
	* @return processing_mode
	*/
	public void setProcessing_mode(String processing_mode) {
		this.processing_mode = processing_mode;
 	}

	/**
	* get多级审批层数
	* @return approvals_up_num
	*/
	public String getApprovals_up_num() {
		return approvals_up_num;
  	}

	/**
	* set多级审批层数
	* @return approvals_up_num
	*/
	public void setApprovals_up_num(String approvals_up_num) {
		this.approvals_up_num = approvals_up_num;
 	}

	/**
	* get多级审批终点角色
	* @return end_approval_role
	*/
	public String getEnd_approval_role() {
		return end_approval_role;
  	}

	/**
	* set多级审批终点角色
	* @return end_approval_role
	*/
	public void setEnd_approval_role(String end_approval_role) {
		this.end_approval_role = end_approval_role;
 	}

	/**
	* get表单部门字段
	* @return form_org
	*/
	public String getForm_org() {
		return form_org;
  	}

	/**
	* set表单部门字段
	* @return form_org
	*/
	public void setForm_org(String form_org) {
		this.form_org = form_org;
 	}

	/**
	* get处理方法
	* @return node_action
	*/
	public String getNode_action() {
		return node_action;
  	}

	/**
	* set处理方法
	* @return node_action
	*/
	public void setNode_action(String node_action) {
		this.node_action = node_action;
 	}

	/**
	* get审批通过下一节点
	* @return node_true
	*/
	public String getNode_true() {
		return node_true;
  	}

	/**
	* set审批通过下一节点
	* @return node_true
	*/
	public void setNode_true(String node_true) {
		this.node_true = node_true;
 	}

	/**
	* get审批未通过下一节点
	* @return node_false
	*/
	public String getNode_false() {
		return node_false;
  	}

	/**
	* set审批未通过下一节点
	* @return node_false
	*/
	public void setNode_false(String node_false) {
		this.node_false = node_false;
 	}

	/**
	* get节点分支条件
	* @return 节点分支条件
	*/
	public List<z_workflow_node_branch> getZ_workflow_node_branch_list() {
		return z_workflow_node_branch_list;
  	}

	/**
	* set节点分支条件
	* @return 节点分支条件
	*/
	public void setZ_workflow_node_branch_list(List<z_workflow_node_branch> z_workflow_node_branch_list) {
		this.z_workflow_node_branch_list = z_workflow_node_branch_list;
 	}

}
