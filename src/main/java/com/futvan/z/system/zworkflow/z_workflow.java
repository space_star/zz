package com.futvan.z.system.zworkflow;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_workflow extends SuperBean{
	//流程名称
	private String w_name;

	//所属表单
	private String w_form;

	//是否启用
	private String is_start;

	//流程节点
	private List<z_workflow_node> z_workflow_node_list;

	/**
	* get流程名称
	* @return w_name
	*/
	public String getW_name() {
		return w_name;
  	}

	/**
	* set流程名称
	* @return w_name
	*/
	public void setW_name(String w_name) {
		this.w_name = w_name;
 	}

	/**
	* get所属表单
	* @return w_form
	*/
	public String getW_form() {
		return w_form;
  	}

	/**
	* set所属表单
	* @return w_form
	*/
	public void setW_form(String w_form) {
		this.w_form = w_form;
 	}

	/**
	* get是否启用
	* @return is_start
	*/
	public String getIs_start() {
		return is_start;
  	}

	/**
	* set是否启用
	* @return is_start
	*/
	public void setIs_start(String is_start) {
		this.is_start = is_start;
 	}

	/**
	* get流程节点
	* @return 流程节点
	*/
	public List<z_workflow_node> getZ_workflow_node_list() {
		return z_workflow_node_list;
  	}

	/**
	* set流程节点
	* @return 流程节点
	*/
	public void setZ_workflow_node_list(List<z_workflow_node> z_workflow_node_list) {
		this.z_workflow_node_list = z_workflow_node_list;
 	}

}
