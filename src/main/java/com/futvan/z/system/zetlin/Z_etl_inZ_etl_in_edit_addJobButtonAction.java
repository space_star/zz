package com.futvan.z.system.zetlin;
import java.util.HashMap;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
@Controller
public class Z_etl_inZ_etl_in_edit_addJobButtonAction extends SuperAction{

	@Autowired
	private CommonService commonService;
	@Autowired
	private ZetlinService zetlinService;

	@RequestMapping(value="/z_etl_in_edit_addJob")
	public @ResponseBody Result z_etl_in_edit_addJob(@RequestParam HashMap<String,String> bean) throws Exception {
		return zetlinService.z_etl_in_edit_addJob(bean,request);
	}
}
