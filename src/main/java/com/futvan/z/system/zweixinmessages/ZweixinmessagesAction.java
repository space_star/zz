package com.futvan.z.system.zweixinmessages;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.HashMap;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.DateUtil;
import com.futvan.z.framework.util.WeiXinUtil;
import com.futvan.z.system.zweixinmessages.ZweixinmessagesService;
@Controller
public class ZweixinmessagesAction extends SuperAction{
	@Autowired
	private ZweixinmessagesService zweixinmessagesService;

	/**
	 * 微信消息入口
	 * @param signature
	 * @param timestamp
	 * @param nonce
	 * @param echostr
	 * @return 
	 * @throws Exception 
	 */
	@RequestMapping(value="/wx")
	public @ResponseBody String wx(@RequestParam HashMap<String,String> bean) throws Exception{
		String result = "";
		String signature = bean.get("signature");
		String timestamp = bean.get("timestamp");
		String nonce = bean.get("nonce");
		String echostr = bean.get("echostr");
		String msg_signature = bean.get("msg_signature");
		if (request.getMethod().equals("POST")) {

			//获取微信发送来的信息
			String info = IOUtils.toString(request.getInputStream());

			//消息解密
			String weixin_request = WeiXinUtil.Decrypt(info,msg_signature,timestamp,nonce);

			//请求信息转换为对象
			z_weixin_messages wxm = WeiXinUtil.XmlToInfo(weixin_request);

			//信息处理并返回
			String return_info_mingwen = handler(wxm);
			if(z.isNotNull(return_info_mingwen)){
				//消息加密并返回
				result = WeiXinUtil.Encryption(return_info_mingwen, timestamp, nonce);
			}
		} else {
			//信息验证
			if (WeiXinUtil.CheckingInfo(signature, timestamp, nonce)) {
				z.Log("微信服务器绑定消息|信息验证成功：signature="+signature+" timestamp="+timestamp+" nonce="+nonce+" echostr="+echostr);
				result = echostr;
			}else{
				z.Log("微信服务器绑定消息|信息验证失败：signature="+signature+" timestamp="+timestamp+" nonce="+nonce+" echostr="+echostr);
			}
		}

		return result;
	}



	/**
	 * 消息处理（核心方法）
	 * @param weixin_request
	 * @return
	 */
	public String handler(z_weixin_messages wx){
		String returnvalue = "";
		//事件消息
		if("event".equals(wx.getMsgType())){

			//关注
			if("subscribe".equals(wx.getEvent())){
				returnvalue = handler_subscribe(wx);
			}

			//取消关注
			if("unsubscribe".equals(wx.getEvent())){
				returnvalue = handler_unsubscribe(wx);
			}

			//点击菜单事件
			if("CLICK".equals(wx.getEvent())){
				returnvalue = handler_click(wx);
			}

			//打开链接事件
			if("VIEW".equals(wx.getEvent())){
				returnvalue = handler_view(wx);
			}

			//模板消息-事件推送
			if("TEMPLATESENDJOBFINISH".equals(wx.getEvent())){
				handler_TEMPLATESENDJOBFINISH(wx);
			}
		}else if("text".equals(wx.getMsgType())){
			//用户发送文件信息
			returnvalue = handler_text(wx);
		}
		return returnvalue;
	}



	//用户发送文本信息
	private String handler_text(z_weixin_messages wx) {
		return null;
	}

	//模板消息-事件推送
	private void handler_TEMPLATESENDJOBFINISH(z_weixin_messages wx) {
		StringBuffer log = new StringBuffer();
		log.append("[发消息公众号:" + wx.getFromUserName() + "]");
		log.append("[接口用户:" + wx.getToUserName() + "]");
		log.append("[发送时间:" + wx.getCreateTime() + "]");
		log.append("[消息ID:" + wx.getMsgId() + "]");
		z.Log("微信公众号向用户发送消息-事件推送|"+log.toString());
	}

	//打开链接事件
	private String handler_view(z_weixin_messages wx) {
		z.Log("公众号用户："+wx.getFromUserName()+"|打开链接|"+wx.getUrl());
		return null;
	}

	//点击菜单事件
	private String handler_click(z_weixin_messages wx) {
		z_weixin_messages returnvalue = new z_weixin_messages();
		returnvalue.setToUserName(wx.getFromUserName());
		returnvalue.setFromUserName(wx.getToUserName());
		returnvalue.setCreateTime(DateUtil.getDateTime() + "");

		if (wx.getEventKey().equals("btn_001")) {
			//returnvalue = ClickHandler.btn_health_report(returnvalue);
			return WeiXinUtil.InfoToXML(returnvalue);
		} else if (wx.getEventKey().equals("btn_hot_product")) {
			//returnvalue = ClickHandler.btn_hot_product(returnvalue);
			return WeiXinUtil.InfoToXML(returnvalue);
		}
		returnvalue.setMsgType("text");
		returnvalue.setContent("点击菜单");
		return WeiXinUtil.InfoToXML(returnvalue);
	}


	//取消关注
	private String handler_unsubscribe(z_weixin_messages wx) {
		z.Log("公众号用户："+wx.getFromUserName()+"|取消了关注");
		return "";
	}

	//关注
	private String handler_subscribe(z_weixin_messages wx) {
		StringBuffer content = new StringBuffer("");
		content.append("Hi~感谢您关注！\n");

		z_weixin_messages returnvalue = new z_weixin_messages();
		returnvalue.setToUserName(wx.getFromUserName());
		returnvalue.setFromUserName(wx.getToUserName());
		returnvalue.setCreateTime(DateUtil.getDateTime()+ "");
		returnvalue.setMsgType("text");
		returnvalue.setContent(content.toString());
		return WeiXinUtil.InfoToXML(returnvalue);
	}
}
