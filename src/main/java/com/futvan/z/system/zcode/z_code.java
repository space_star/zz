package com.futvan.z.system.zcode;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_code extends SuperBean{
	//字典编号
	private String z_number;

	//字典名称
	private String z_name;

	//所有键值对
	private String all_key_value;

	//引用字段
	private String columns;

	//字典明细
	private List<z_code_detail> z_code_detail_list;

	/**
	* get字典编号
	* @return z_number
	*/
	public String getZ_number() {
		return z_number;
  	}

	/**
	* set字典编号
	* @return z_number
	*/
	public void setZ_number(String z_number) {
		this.z_number = z_number;
 	}

	/**
	* get字典名称
	* @return z_name
	*/
	public String getZ_name() {
		return z_name;
  	}

	/**
	* set字典名称
	* @return z_name
	*/
	public void setZ_name(String z_name) {
		this.z_name = z_name;
 	}

	/**
	* get所有键值对
	* @return all_key_value
	*/
	public String getAll_key_value() {
		return all_key_value;
  	}

	/**
	* set所有键值对
	* @return all_key_value
	*/
	public void setAll_key_value(String all_key_value) {
		this.all_key_value = all_key_value;
 	}

	/**
	* get引用字段
	* @return columns
	*/
	public String getColumns() {
		return columns;
  	}

	/**
	* set引用字段
	* @return columns
	*/
	public void setColumns(String columns) {
		this.columns = columns;
 	}

	/**
	* get字典明细
	* @return 字典明细
	*/
	public List<z_code_detail> getZ_code_detail_list() {
		return z_code_detail_list;
  	}

	/**
	* set字典明细
	* @return 字典明细
	*/
	public void setZ_code_detail_list(List<z_code_detail> z_code_detail_list) {
		this.z_code_detail_list = z_code_detail_list;
 	}

}
