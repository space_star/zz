package com.futvan.z.system.zworkjob;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_workjob_approvaluser extends SuperBean{
	//审批人
	private String userzid;

	//是否处理
	private String is_approval;

	//审批结果
	private String examine_state;

	//审批意见
	private String examine_opinion;

	/**
	* get审批人
	* @return userzid
	*/
	public String getUserzid() {
		return userzid;
  	}

	/**
	* set审批人
	* @return userzid
	*/
	public void setUserzid(String userzid) {
		this.userzid = userzid;
 	}

	/**
	* get是否处理
	* @return is_approval
	*/
	public String getIs_approval() {
		return is_approval;
  	}

	/**
	* set是否处理
	* @return is_approval
	*/
	public void setIs_approval(String is_approval) {
		this.is_approval = is_approval;
 	}

	/**
	* get审批结果
	* @return examine_state
	*/
	public String getExamine_state() {
		return examine_state;
  	}

	/**
	* set审批结果
	* @return examine_state
	*/
	public void setExamine_state(String examine_state) {
		this.examine_state = examine_state;
 	}

	/**
	* get审批意见
	* @return examine_opinion
	*/
	public String getExamine_opinion() {
		return examine_opinion;
  	}

	/**
	* set审批意见
	* @return examine_opinion
	*/
	public void setExamine_opinion(String examine_opinion) {
		this.examine_opinion = examine_opinion;
 	}

}
