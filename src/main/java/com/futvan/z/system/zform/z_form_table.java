package com.futvan.z.system.zform;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_form_table extends SuperBean{
	//表ID
	private String table_id;

	//表名称
	private String table_title;

	//上级表
	private String parent_table_id;

	//排序条件
	private String orderby;

	//排序模式
	private String orderby_pattern;

	//子表默认收缩
	private String is_collapsed;

	//编辑页面布局
	private String edit_page_layout;

	//列表显示行数
	private String default_rowcount;

	//双击行执行
	private String list_dblclick;

	//列表形式
	private String list_display_style;

	//卡片规格
	private String card_size;

	//列表加载方法
	private String list_action;

	//是否启用默认查询接口
	private String is_open_default_getservice;

	//是否启用默认保存接口
	private String is_open_default_saveservice;

	//是否启用默认删除接口
	private String is_open_default_deleteservice;

	//列表页面初始化JS
	private String list_ready_js;

	//列表页面BODY_JS代码
	private String list_body_js;

	//编辑页面初始化JS
	private String edit_ready_js;

	//编辑页面BODY_JS代码
	private String edit_body_js;

	//列表页面初始HTML
	private String list_ready_html;

	//编辑页面初始HTML
	private String edit_ready_html;

	//字段管理
	private List<z_form_table_column> z_form_table_column_list;

	//按钮管理
	private List<z_form_table_button> z_form_table_button_list;
	
	//明细表
	private List<z_form_table> z_form_table_detail_list;

	/**
	* get表ID
	* @return table_id
	*/
	public String getTable_id() {
		return table_id;
  	}

	/**
	* set表ID
	* @return table_id
	*/
	public void setTable_id(String table_id) {
		this.table_id = table_id;
 	}

	/**
	* get表名称
	* @return table_title
	*/
	public String getTable_title() {
		return table_title;
  	}

	/**
	* set表名称
	* @return table_title
	*/
	public void setTable_title(String table_title) {
		this.table_title = table_title;
 	}

	/**
	* get上级表
	* @return parent_table_id
	*/
	public String getParent_table_id() {
		return parent_table_id;
  	}

	/**
	* set上级表
	* @return parent_table_id
	*/
	public void setParent_table_id(String parent_table_id) {
		this.parent_table_id = parent_table_id;
 	}

	/**
	* get排序条件
	* @return orderby
	*/
	public String getOrderby() {
		return orderby;
  	}

	/**
	* set排序条件
	* @return orderby
	*/
	public void setOrderby(String orderby) {
		this.orderby = orderby;
 	}

	/**
	* get排序模式
	* @return orderby_pattern
	*/
	public String getOrderby_pattern() {
		return orderby_pattern;
  	}

	/**
	* set排序模式
	* @return orderby_pattern
	*/
	public void setOrderby_pattern(String orderby_pattern) {
		this.orderby_pattern = orderby_pattern;
 	}

	/**
	* get子表默认收缩
	* @return is_collapsed
	*/
	public String getIs_collapsed() {
		return is_collapsed;
  	}

	/**
	* set子表默认收缩
	* @return is_collapsed
	*/
	public void setIs_collapsed(String is_collapsed) {
		this.is_collapsed = is_collapsed;
 	}

	/**
	* get编辑页面布局
	* @return edit_page_layout
	*/
	public String getEdit_page_layout() {
		return edit_page_layout;
  	}

	/**
	* set编辑页面布局
	* @return edit_page_layout
	*/
	public void setEdit_page_layout(String edit_page_layout) {
		this.edit_page_layout = edit_page_layout;
 	}

	/**
	* get列表显示行数
	* @return default_rowcount
	*/
	public String getDefault_rowcount() {
		return default_rowcount;
  	}

	/**
	* set列表显示行数
	* @return default_rowcount
	*/
	public void setDefault_rowcount(String default_rowcount) {
		this.default_rowcount = default_rowcount;
 	}

	/**
	* get双击行执行
	* @return list_dblclick
	*/
	public String getList_dblclick() {
		return list_dblclick;
  	}

	/**
	* set双击行执行
	* @return list_dblclick
	*/
	public void setList_dblclick(String list_dblclick) {
		this.list_dblclick = list_dblclick;
 	}

	/**
	* get列表形式
	* @return list_display_style
	*/
	public String getList_display_style() {
		return list_display_style;
  	}

	/**
	* set列表形式
	* @return list_display_style
	*/
	public void setList_display_style(String list_display_style) {
		this.list_display_style = list_display_style;
 	}

	/**
	* get卡片规格
	* @return card_size
	*/
	public String getCard_size() {
		return card_size;
  	}

	/**
	* set卡片规格
	* @return card_size
	*/
	public void setCard_size(String card_size) {
		this.card_size = card_size;
 	}

	/**
	* get列表加载方法
	* @return list_action
	*/
	public String getList_action() {
		return list_action;
  	}

	/**
	* set列表加载方法
	* @return list_action
	*/
	public void setList_action(String list_action) {
		this.list_action = list_action;
 	}

	/**
	* get是否启用默认查询接口
	* @return is_open_default_getservice
	*/
	public String getIs_open_default_getservice() {
		return is_open_default_getservice;
  	}

	/**
	* set是否启用默认查询接口
	* @return is_open_default_getservice
	*/
	public void setIs_open_default_getservice(String is_open_default_getservice) {
		this.is_open_default_getservice = is_open_default_getservice;
 	}

	/**
	* get是否启用默认保存接口
	* @return is_open_default_saveservice
	*/
	public String getIs_open_default_saveservice() {
		return is_open_default_saveservice;
  	}

	/**
	* set是否启用默认保存接口
	* @return is_open_default_saveservice
	*/
	public void setIs_open_default_saveservice(String is_open_default_saveservice) {
		this.is_open_default_saveservice = is_open_default_saveservice;
 	}

	/**
	* get是否启用默认删除接口
	* @return is_open_default_deleteservice
	*/
	public String getIs_open_default_deleteservice() {
		return is_open_default_deleteservice;
  	}

	/**
	* set是否启用默认删除接口
	* @return is_open_default_deleteservice
	*/
	public void setIs_open_default_deleteservice(String is_open_default_deleteservice) {
		this.is_open_default_deleteservice = is_open_default_deleteservice;
 	}

	/**
	* get列表页面初始化JS
	* @return list_ready_js
	*/
	public String getList_ready_js() {
		return list_ready_js;
  	}

	/**
	* set列表页面初始化JS
	* @return list_ready_js
	*/
	public void setList_ready_js(String list_ready_js) {
		this.list_ready_js = list_ready_js;
 	}

	/**
	* get列表页面BODY_JS代码
	* @return list_body_js
	*/
	public String getList_body_js() {
		return list_body_js;
  	}

	/**
	* set列表页面BODY_JS代码
	* @return list_body_js
	*/
	public void setList_body_js(String list_body_js) {
		this.list_body_js = list_body_js;
 	}

	/**
	* get编辑页面初始化JS
	* @return edit_ready_js
	*/
	public String getEdit_ready_js() {
		return edit_ready_js;
  	}

	/**
	* set编辑页面初始化JS
	* @return edit_ready_js
	*/
	public void setEdit_ready_js(String edit_ready_js) {
		this.edit_ready_js = edit_ready_js;
 	}

	/**
	* get编辑页面BODY_JS代码
	* @return edit_body_js
	*/
	public String getEdit_body_js() {
		return edit_body_js;
  	}

	/**
	* set编辑页面BODY_JS代码
	* @return edit_body_js
	*/
	public void setEdit_body_js(String edit_body_js) {
		this.edit_body_js = edit_body_js;
 	}

	/**
	* get列表页面初始HTML
	* @return list_ready_html
	*/
	public String getList_ready_html() {
		return list_ready_html;
  	}

	/**
	* set列表页面初始HTML
	* @return list_ready_html
	*/
	public void setList_ready_html(String list_ready_html) {
		this.list_ready_html = list_ready_html;
 	}

	/**
	* get编辑页面初始HTML
	* @return edit_ready_html
	*/
	public String getEdit_ready_html() {
		return edit_ready_html;
  	}

	/**
	* set编辑页面初始HTML
	* @return edit_ready_html
	*/
	public void setEdit_ready_html(String edit_ready_html) {
		this.edit_ready_html = edit_ready_html;
 	}

	/**
	* get字段管理
	* @return 字段管理
	*/
	public List<z_form_table_column> getZ_form_table_column_list() {
		return z_form_table_column_list;
  	}

	/**
	* set字段管理
	* @return 字段管理
	*/
	public void setZ_form_table_column_list(List<z_form_table_column> z_form_table_column_list) {
		this.z_form_table_column_list = z_form_table_column_list;
 	}

	/**
	* get按钮管理
	* @return 按钮管理
	*/
	public List<z_form_table_button> getZ_form_table_button_list() {
		return z_form_table_button_list;
  	}

	/**
	* set按钮管理
	* @return 按钮管理
	*/
	public void setZ_form_table_button_list(List<z_form_table_button> z_form_table_button_list) {
		this.z_form_table_button_list = z_form_table_button_list;
 	}

	public List<z_form_table> getZ_form_table_detail_list() {
		return z_form_table_detail_list;
	}

	public void setZ_form_table_detail_list(List<z_form_table> z_form_table_detail_list) {
		this.z_form_table_detail_list = z_form_table_detail_list;
	}
	
}
