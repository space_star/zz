package com.futvan.z.system.zorg;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_org extends SuperBean{
	//组织类型
	private String org_type;

	//组织编号
	private String org_id;

	//组织名称
	private String org_name;

	//所属地区
	private String area_id;

	//负责人
	private String user_head;

	//上级组织
	private String parentid;

	//完整组织名称
	private String full_org_name;

	//组织用户
	private List<z_org_user> z_org_user_list;

	/**
	* get组织类型
	* @return org_type
	*/
	public String getOrg_type() {
		return org_type;
  	}

	/**
	* set组织类型
	* @return org_type
	*/
	public void setOrg_type(String org_type) {
		this.org_type = org_type;
 	}

	/**
	* get组织编号
	* @return org_id
	*/
	public String getOrg_id() {
		return org_id;
  	}

	/**
	* set组织编号
	* @return org_id
	*/
	public void setOrg_id(String org_id) {
		this.org_id = org_id;
 	}

	/**
	* get组织名称
	* @return org_name
	*/
	public String getOrg_name() {
		return org_name;
  	}

	/**
	* set组织名称
	* @return org_name
	*/
	public void setOrg_name(String org_name) {
		this.org_name = org_name;
 	}

	/**
	* get所属地区
	* @return area_id
	*/
	public String getArea_id() {
		return area_id;
  	}

	/**
	* set所属地区
	* @return area_id
	*/
	public void setArea_id(String area_id) {
		this.area_id = area_id;
 	}

	/**
	* get负责人
	* @return user_head
	*/
	public String getUser_head() {
		return user_head;
  	}

	/**
	* set负责人
	* @return user_head
	*/
	public void setUser_head(String user_head) {
		this.user_head = user_head;
 	}

	/**
	* get上级组织
	* @return parentid
	*/
	public String getParentid() {
		return parentid;
  	}

	/**
	* set上级组织
	* @return parentid
	*/
	public void setParentid(String parentid) {
		this.parentid = parentid;
 	}

	/**
	* get完整组织名称
	* @return full_org_name
	*/
	public String getFull_org_name() {
		return full_org_name;
  	}

	/**
	* set完整组织名称
	* @return full_org_name
	*/
	public void setFull_org_name(String full_org_name) {
		this.full_org_name = full_org_name;
 	}

	/**
	* get组织用户
	* @return 组织用户
	*/
	public List<z_org_user> getZ_org_user_list() {
		return z_org_user_list;
  	}

	/**
	* set组织用户
	* @return 组织用户
	*/
	public void setZ_org_user_list(List<z_org_user> z_org_user_list) {
		this.z_org_user_list = z_org_user_list;
 	}

}
