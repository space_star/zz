package com.futvan.z.system.zmessages;
import org.springframework.stereotype.Service;
import com.futvan.z.framework.core.SuperService;
@Service
public class ZmessagesService extends SuperService{

	/**
	 * 获取指定用户发来的消息数量
	 * @param fromuserid
	 * @param touserid 
	 * @return
	 */
	public int getFromuserMessageCount(String fromuserid, String touserid) {
		return selectInt("select count(*) from z_messages where fromuserid = '"+fromuserid+"' and touserid = '"+touserid+"'   and is_open = '0'");
	}

	
	public int UpdateIsOpen(String fromuserid, String touserid) {
		// TODO 自动生成的方法存根
		return update("update z_messages set is_open = '1' where fromuserid = '"+fromuserid+"' and touserid = '"+touserid+"' and is_open = '0' ");
	}

}
