package com.futvan.z.system.zmessages;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_messages extends SuperBean{
	//发送人
	private String fromuserid;

	//发送人姓名
	private String fromusername;

	//接收人
	private String touserid;

	//接收人姓名
	private String tousername;

	//通信命令
	private String command_type;

	//消息类型
	private String msgtype;

	//消息
	private String msg;

	//发送时间
	private String sendtime;

	//接收时间
	private String receivingtime;

	//是否收到消息
	private String is_receiving;

	//是否查看消息
	private String is_open;

	/**
	* get发送人
	* @return fromuserid
	*/
	public String getFromuserid() {
		return fromuserid;
  	}

	/**
	* set发送人
	* @return fromuserid
	*/
	public void setFromuserid(String fromuserid) {
		this.fromuserid = fromuserid;
 	}

	/**
	* get发送人姓名
	* @return fromusername
	*/
	public String getFromusername() {
		return fromusername;
  	}

	/**
	* set发送人姓名
	* @return fromusername
	*/
	public void setFromusername(String fromusername) {
		this.fromusername = fromusername;
 	}

	/**
	* get接收人
	* @return touserid
	*/
	public String getTouserid() {
		return touserid;
  	}

	/**
	* set接收人
	* @return touserid
	*/
	public void setTouserid(String touserid) {
		this.touserid = touserid;
 	}

	/**
	* get接收人姓名
	* @return tousername
	*/
	public String getTousername() {
		return tousername;
  	}

	/**
	* set接收人姓名
	* @return tousername
	*/
	public void setTousername(String tousername) {
		this.tousername = tousername;
 	}

	/**
	* get通信命令
	* @return command_type
	*/
	public String getCommand_type() {
		return command_type;
  	}

	/**
	* set通信命令
	* @return command_type
	*/
	public void setCommand_type(String command_type) {
		this.command_type = command_type;
 	}

	/**
	* get消息类型
	* @return msgtype
	*/
	public String getMsgtype() {
		return msgtype;
  	}

	/**
	* set消息类型
	* @return msgtype
	*/
	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
 	}

	/**
	* get消息
	* @return msg
	*/
	public String getMsg() {
		return msg;
  	}

	/**
	* set消息
	* @return msg
	*/
	public void setMsg(String msg) {
		this.msg = msg;
 	}

	/**
	* get发送时间
	* @return sendtime
	*/
	public String getSendtime() {
		return sendtime;
  	}

	/**
	* set发送时间
	* @return sendtime
	*/
	public void setSendtime(String sendtime) {
		this.sendtime = sendtime;
 	}

	/**
	* get接收时间
	* @return receivingtime
	*/
	public String getReceivingtime() {
		return receivingtime;
  	}

	/**
	* set接收时间
	* @return receivingtime
	*/
	public void setReceivingtime(String receivingtime) {
		this.receivingtime = receivingtime;
 	}

	/**
	* get是否收到消息
	* @return is_receiving
	*/
	public String getIs_receiving() {
		return is_receiving;
  	}

	/**
	* set是否收到消息
	* @return is_receiving
	*/
	public void setIs_receiving(String is_receiving) {
		this.is_receiving = is_receiving;
 	}

	/**
	* get是否查看消息
	* @return is_open
	*/
	public String getIs_open() {
		return is_open;
  	}

	/**
	* set是否查看消息
	* @return is_open
	*/
	public void setIs_open(String is_open) {
		this.is_open = is_open;
 	}

}
