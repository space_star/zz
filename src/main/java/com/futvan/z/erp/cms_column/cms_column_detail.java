package com.futvan.z.erp.cms_column;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class cms_column_detail extends SuperBean{
	//图片
	private String img;

	//文件
	private String file;

	/**
	* get图片
	* @return img
	*/
	public String getImg() {
		return img;
  	}

	/**
	* set图片
	* @return img
	*/
	public void setImg(String img) {
		this.img = img;
 	}

	/**
	* get文件
	* @return file
	*/
	public String getFile() {
		return file;
  	}

	/**
	* set文件
	* @return file
	*/
	public void setFile(String file) {
		this.file = file;
 	}

}
