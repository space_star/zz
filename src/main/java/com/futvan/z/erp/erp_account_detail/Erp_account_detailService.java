package com.futvan.z.erp.erp_account_detail;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.futvan.z.erp.erp_account_type.erp_account_type;
import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.MathUtil;
import com.futvan.z.framework.util.StringUtil;
@Service
public class Erp_account_detailService extends SuperService{
	
	/**
	 * 查询方法
	 * @param bean
	 * @param string
	 * @return
	 * @throws Exception 
	 */
	public ModelAndView select(HashMap<String,String> bean,String viewName) throws Exception {
		ModelAndView model = new ModelAndView(viewName);
		if(!z.isNotNull(bean.get("pagenum")) || !z.isNotNull(bean.get("rowcount"))) {
			bean.put("pagenum", "1");
			bean.put("rowcount", z.sp.get("rowcount"));
		}
		
		//获取列表，并执行格式化
		List<HashMap<String,String>> list = selectList(bean);
		for (HashMap<String, String> map : list) {
			String str_a_type = map.get("a_type");
			if(z.isNotNull(str_a_type)) {
				//获取账户类型对象
				erp_account_type at = z.account_type.get(str_a_type);
				if(z.isNotNull(at)) {
					
					//格式化数量
					String str_amount =String.valueOf(map.get("amount"));
					if(z.isNotNull(str_amount)) {
						//格式化数字
						if(z.isNotNull(at.getDecimals()) && StringUtil.isBigDecimal(str_amount)) {
							map.put("amount", MathUtil.FormatNumber(str_amount, new Integer(at.getDecimals())));
						}
					}
					
					//格式化可用数量
					String str_balance =String.valueOf(map.get("balance"));
					if(z.isNotNull(str_balance)) {
						//格式化数字
						if(z.isNotNull(at.getDecimals()) && StringUtil.isBigDecimal(str_balance)) {
							map.put("balance", MathUtil.FormatNumber(str_balance, new Integer(at.getDecimals())));
						}
					}
					
					//格式化冻结数量
					String str_frozen =String.valueOf(map.get("frozen"));
					if(z.isNotNull(str_frozen)) {
						//格式化数字
						if(z.isNotNull(at.getDecimals()) && StringUtil.isBigDecimal(str_frozen)) {
							map.put("frozen", MathUtil.FormatNumber(str_frozen, new Integer(at.getDecimals())));
						}
					}
					
					
					
					//格式化总账可用数量
					String str_all_balance =String.valueOf(map.get("all_balance"));
					if(z.isNotNull(str_all_balance)) {
						//格式化数字
						if(z.isNotNull(at.getDecimals()) && StringUtil.isBigDecimal(str_all_balance)) {
							map.put("all_balance", MathUtil.FormatNumber(str_all_balance, new Integer(at.getDecimals())));
						}
					}
					
					
					//格式化总账冻结数量
					String str_all_frozen =String.valueOf(map.get("all_frozen"));
					if(z.isNotNull(str_all_frozen)) {
						//格式化数字
						if(z.isNotNull(at.getDecimals()) && StringUtil.isBigDecimal(str_all_frozen)) {
							map.put("all_frozen", MathUtil.FormatNumber(str_all_frozen, new Integer(at.getDecimals())));
						}
					}
					
				}
			}
		}
		model.addObject("list", list);
		bean.put("queryinfolist", getQueryInfoList(bean));//常用用户该表单查询条件
		model.addObject("bean", bean);
		return model;
	}
	
}
