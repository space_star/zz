package com.futvan.z.erp.erp_account;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.futvan.z.erp.erp_account_detail.erp_account_detail;
import com.futvan.z.erp.erp_account_outdetail.erp_account_outdetail;
import com.futvan.z.erp.erp_account_type.erp_account_type;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.MathUtil;
import com.futvan.z.framework.util.StringUtil;
@Service
public class Erp_accountService extends SuperService{
	
	/**
	 * 进账
	 * @param userid 客户ID
	 * @param a_type 账户类型
	 * @param amount 数量
	 * @param mode 模式【如果为空默认为直接入账】
	 * @param bizid 业务ID
	 * @return 
	 * @throws Exception 
	 */
	public Result Add(String userid,String a_type,String amount,String mode,String bizid) throws Exception {
		Result result = new Result();
		if(z.isNotNull(userid)) {
			if(z.isNotNull(a_type)) {
				erp_account_type at = z.account_type.get(a_type);
				if(z.isNotNull(at)) {
					if(z.isNotNull(amount)) {
						if(StringUtil.isBigDecimal(amount)) {
							//格式化数字
							if(z.isNotNull(at.getDecimals()) && StringUtil.isBigDecimal(amount)) {
								amount = MathUtil.FormatNumber(amount, new Integer(at.getDecimals()));
							}
							
							//如果入账模式为空，默认为0直接入账,1为冻结入账
							if(z.isNull(mode)) {
								mode = "0";
							}
							if(!"0".equals(mode) && !"1".equals(mode)) {
								z.Exception("非法操作模式，无法入账|【"+mode+"】");
							}

							//修改总账
							UpdateAccountIN(userid, a_type, amount, mode,at);

							//获取最新总账记录
							erp_account ea = sqlSession.selectOne("erp_account_select_sql","select * from erp_account where userid = '"+userid+"' and a_type = '"+a_type+"'");
							if(z.isNotNull(ea)) {
								//插入明细账记录
								erp_account_detail ad = new erp_account_detail();
								String inid = z.newZid("erp_account_detail");
								ad.setZid(inid);
								ad.setNumber(inid);
								ad.setUserid(userid);
								ad.setA_type(a_type);
								ad.setIn_out("0");//0:入账
								ad.setOperation_mode(mode);//操作模式 0：直接入账 1：冻结入账
								ad.setAmount(amount);//数量
								ad.setUnit(at.getUnit());//单位
								if(z.isNotNull(bizid)) {
									ad.setBizid(bizid);//业务ID
								}
								if("0".equals(mode)) {//直接入账模式
									ad.setBalance(amount);//可用数量=入账数量
									ad.setFrozen("0");//冻结数量
								}else if("1".equals(mode)) {//冻结模式
									ad.setBalance("0");//可用数量
									ad.setFrozen(amount);//冻结数量=入账数量
								}else {
									z.Exception("非法操作模式，无法入账|【"+mode+"】");
								}
								ad.setAll_balance(ea.getBalance());//总账可用数量
								ad.setAll_frozen(ea.getFrozen());//总账冻结数量
								insert("erp_account_detail_insert", ad);

								//设置返回信息
								result.setCode(Code.SUCCESS);
								result.setMsg("成功进账【"+amount+" "+z.codeValue.get("unit_"+at.getUnit())+"】");
								result.setData(inid);
							}else {
								z.Exception("无法获取最新总账记录");
							}
						}else {
							result.setCode(Code.ERROR);
							result.setMsg("入账数量无法转换成数值，无法入账");
						}
					}else {
						result.setCode(Code.ERROR);
						result.setMsg("入账数量不可为空");
					}
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("【"+a_type+"】 无此账户类型");
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("账户类型不可为空");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("客户ID不可为空");
		}
		return result;
	}

	/**
	 * 出账
	 * @param userid
	 * @param a_type
	 * @param amount
	 * @param mode
	 * @param bizid
	 * @return
	 * @throws Exception 
	 */
	public Result Subtract(String userid,String a_type,String amount,String mode,String bizid) throws Exception {
		Result result = new Result();
		if(z.isNotNull(userid)) {
			if(z.isNotNull(a_type)) {
				erp_account_type at = z.account_type.get(a_type);
				if(z.isNotNull(at)) {
					if(z.isNotNull(amount)) {
						if(StringUtil.isBigDecimal(amount)) {
							//格式化数字
							if(z.isNotNull(at.getDecimals()) && StringUtil.isBigDecimal(amount)) {
								amount = MathUtil.FormatNumber(amount, new Integer(at.getDecimals()));
							}
							
							//如果入账模式为空，默认为0直接入账,1为冻结入账
							if(z.isNull(mode)) {
								mode = "0";
							}
							if(!"0".equals(mode) && !"1".equals(mode)) {
								z.Exception("非法操作模式，无法入账|【"+mode+"】");
							}


							//判读可用余额够支持扣减
							StringBuffer available_balance_sql = new StringBuffer();
							available_balance_sql.append(" SELECT IFNULL(SUM(balance),0) balance  ");
							available_balance_sql.append(" FROM erp_account_detail   ");
							available_balance_sql.append(" WHERE userid = '"+userid+"' AND a_type = '"+a_type+"' AND in_out = '0' AND balance > 0  ");
							int available_balance = selectInt(available_balance_sql.toString());
							BigDecimal available_balance_bigdecimal = new BigDecimal(available_balance);
							BigDecimal amount_bigdecimal = new BigDecimal(amount);
							//判断可用余额是否大于本次出账数量
							if(available_balance_bigdecimal.compareTo(amount_bigdecimal)>=0) {
								//===============插入明细账记录===============
								erp_account_detail ad = new erp_account_detail();
								String outid = z.newZid("erp_account_detail");
								ad.setZid(outid);
								ad.setNumber(outid);
								ad.setUserid(userid);
								ad.setA_type(a_type);
								ad.setIn_out("1");//1：出账
								ad.setOperation_mode(mode);//操作模式 0：直接入账 1：冻结入账
								ad.setAmount(amount);//数量
								ad.setUnit(at.getUnit());//单位
								if("1".equals(mode)) {//冻结模式
									ad.setBalance("0");//可用数量
									ad.setFrozen(amount);//冻结数量=入账数量
								}else {
									ad.setBalance("0");//可用数量
									ad.setFrozen("0");//冻结数量
								}
								if(z.isNotNull(bizid)) {
									ad.setBizid(bizid);//业务ID
								}
								insert("erp_account_detail_insert", ad);
								//===============插入出账关联入账明细===============
								//获取所有可以入账记录
								StringBuffer inListSQL = new StringBuffer();
								inListSQL.append(" SELECT *  ");
								inListSQL.append(" FROM erp_account_detail   ");
								inListSQL.append(" WHERE userid = '"+userid+"' AND a_type = '"+a_type+"' AND in_out = '0' AND balance > 0  ");
								inListSQL.append(" ORDER BY create_time asc ");
								List<erp_account_detail> inList = selectList("erp_account_detail_select_sql", inListSQL);
								//遍历入账记录
								for (erp_account_detail inAccount : inList) {
									//获取当前入账记录的可用数量
									BigDecimal detail_balance_bigdecimal = new BigDecimal(inAccount.getBalance());
									if(detail_balance_bigdecimal.compareTo(amount_bigdecimal)>=0) {//当前记录可用数量够本次扣减的数量
										//保存出账关联入账记录
										erp_account_outdetail account_out_in = new erp_account_outdetail();
										account_out_in.setZid(z.newZid("erp_account_outdetail"));
										account_out_in.setOutid(outid);//出账记录ID
										account_out_in.setInid(inAccount.getZid());//入账ID
										account_out_in.setOperation_mode(mode);//设置模式
										account_out_in.setA_type(inAccount.getA_type());//账户类型
										account_out_in.setAmount(amount_bigdecimal.toString());//出账关联入账本条数量
										account_out_in.setUnit(inAccount.getUnit());//单位
										
										int insertnum = insert("erp_account_outdetail_insert", account_out_in);
										if(insertnum!=1) {
											z.Exception("保存出账关联入账记录出错");
										}
										//新的可以数量
										BigDecimal new_balance = detail_balance_bigdecimal.subtract(amount_bigdecimal);
										if(new_balance.compareTo(new BigDecimal(0))<0) {
											z.Exception("可用数量不足，无法出账");
										}
										if("0".equals(mode)) {//直接入账
											int updatenum = update("update erp_account_detail set balance = "+new_balance+",zversion = zversion +1  where zid = '"+inAccount.getZid()+"' and zversion = "+inAccount.getZversion()+" ");
											if(updatenum!=1) {
												z.Exception("出账记录关联更新入账记录可以数量出错|入账数量已发生变更，请重新操作");
											}
										}else if("1".equals(mode)) {//冻结入账
											//计算出新的冻结数量，新的冻结数量=旧的冻结数量+本次出账数量
											BigDecimal old_frozen = new BigDecimal(inAccount.getFrozen());
											BigDecimal new_frozen = old_frozen.add(amount_bigdecimal);
											int updatenum = update("update erp_account_detail set balance = "+new_balance+",frozen = "+new_frozen+",zversion = zversion +1  where zid = '"+inAccount.getZid()+"' and zversion = "+inAccount.getZversion()+" ");
											if(updatenum!=1) {
												z.Exception("出账记录关联更新入账记录可以数量出错|入账数量已发生变更，请重新操作");
											}
										}else {
											z.Exception("非法入账操作模式|"+mode);
										}
										//跳出循环
										break;
									}else {//当前入账记录的可用数量小于本次出账数量
										//保存出账关联入账记录
										erp_account_outdetail account_out_in = new erp_account_outdetail();
										account_out_in.setZid(z.newZid("erp_account_outdetail"));
										account_out_in.setOutid(outid);//出账记录ID
										account_out_in.setInid(inAccount.getZid());//入账ID
										account_out_in.setOperation_mode(mode);//设置模式
										account_out_in.setAmount(detail_balance_bigdecimal.toString());//出账关联入账本条数量
										int insertnum = insert("erp_account_outdetail_insert", account_out_in);
										if(insertnum!=1) {
											z.Exception("保存出账关联入账记录出错");
										}
										//剩余需要扣减的数量 = 本次出账扣减数量 - 本条入账可以数量  
										amount_bigdecimal = amount_bigdecimal.subtract(detail_balance_bigdecimal);
										if(amount_bigdecimal.compareTo(new BigDecimal(0))<0) {
											z.Exception("计算过程产生负数，无法出账");
										}
										//扣减当前记录的可以数量变成0
										if("0".equals(mode)) {//直接入账
											int updatenum = update("update erp_account_detail set balance = 0 ,zversion = zversion +1  where zid = '"+inAccount.getZid()+"' and zversion = "+inAccount.getZversion()+" ");
											if(updatenum!=1) {
												z.Exception("出账记录关联更新入账记录可以数量出错|入账数量已发生变更，请重新操作");
											}
										}else if("1".equals(mode)) {//冻结入账
											//计算出新的冻结数量，新的冻结数量=旧的冻结数量+本次出账数量
											BigDecimal old_frozen = new BigDecimal(inAccount.getFrozen());
											BigDecimal new_frozen = old_frozen.add(detail_balance_bigdecimal);
											int updatenum = update("update erp_account_detail set balance = 0 ,frozen = "+new_frozen+",zversion = zversion +1  where zid = '"+inAccount.getZid()+"' and zversion = "+inAccount.getZversion()+" ");
											if(updatenum!=1) {
												z.Exception("出账记录关联更新入账记录可以数量出错|入账数量已发生变更，请重新操作");
											}
										}else {
											z.Exception("非法入账操作模式|"+mode);
										}
										//跳出本次循环，进入下一次循环
										continue;
									}
								}
								//===============修改总账===============
								UpdateAccountOUT(userid, a_type, amount, mode,at);

								//===============修改明细账中总账可用数量与冻结数量===============
								//获取最新总账记录
								erp_account ea = sqlSession.selectOne("erp_account_select_sql","select * from erp_account where userid = '"+userid+"' and a_type = '"+a_type+"'");
								if(z.isNotNull(ea)) {
									if(z.isNull(ea.getBalance())) {
										ea.setBalance("0");
									}
									if(z.isNull(ea.getFrozen())) {
										ea.setFrozen("0");
									}
									//修改明细记录
									erp_account_detail out_update_allbalance = sqlSession.selectOne("erp_account_detail_select_sql", "select * from erp_account_detail where zid = '"+outid+"'");
									if(z.isNotNull(out_update_allbalance)) {
										int updatenum = update("update erp_account_detail set all_balance = "+ea.getBalance()+" ,all_frozen = "+ea.getFrozen()+",zversion = zversion +1  where zid = '"+out_update_allbalance.getZid()+"' and zversion = "+out_update_allbalance.getZversion()+" ");
										if(updatenum!=1) {
											z.Exception("修改明细账中总账可用数量与冻结数量出错");
										}
									}else {
										z.Exception("修改明细账中总账可用数量与冻结数量出错|获取出账明细记录为空");
									}
								}else {
									z.Exception("无法获取最新总账记录");
								}

								//设置返回信息
								result.setCode(Code.SUCCESS);
								result.setMsg("成功出账【"+amount+" "+z.codeValue.get("unit_"+at.getUnit())+"】");
								result.setData(outid);


							}else {
								result.setCode(Code.ERROR);
								result.setMsg("余额不足");
							}
						}else {
							result.setCode(Code.ERROR);
							result.setMsg("入账数量无法转换成数值，无法入账");
						}
					}else {
						result.setCode(Code.ERROR);
						result.setMsg("入账数量不可为空");
					}
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("【"+a_type+"】 无此账户类型");
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("账户类型不可为空");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("客户ID不可为空");
		}
		return result;
	}
	
	/**
	 * 
	 * 删除明细账，该明细账必须满足如下条件方可删除
	 * 1.操作模式必须为冻结操作模式
	 * 2.是否解冻必须为未解冻
	 * 3.数量与冻结数量必须相等
	 * @param adid 明细账ID
	 * @return
	 * @throws Exception
	 */
	public Result Remove(String adid) throws Exception{
		Result result = new Result();
		if(z.isNotNull(adid)) {
			//根据明细账ID找到明细账对象
			erp_account_detail ad = sqlSession.selectOne("erp_account_detail_select_zid", adid);
			if(z.isNotNull(ad)) {
				//操作模式必须为冻结模式
				if("1".equals(ad.getOperation_mode())) {
					//是否解冻必须为未解冻
					if("0".equals(ad.getIsunfrozen())) {
						//数量
						BigDecimal amount_bigdecimal = new BigDecimal(ad.getAmount());
						//冻结数量
						BigDecimal frozen_bigdecimal = new BigDecimal(ad.getFrozen());
						if(amount_bigdecimal.compareTo(frozen_bigdecimal)==0) {
							//=============================删除明细账记录=============================
							int deletenum = sqlSession.delete("delete", "delete from erp_account_detail where zid = '"+ad.getZid()+"' and zversion = "+ad.getZversion()+" ");
							if(deletenum!=1) {
								z.Exception("删除明细记录出错|删除记录条数"+deletenum);
							}
							//=============================清空总账记录=============================
							List<erp_account> userAccountList = selectList("erp_account_select_sql","select * from erp_account where userid = '"+ad.getUserid()+"' and a_type = '"+ad.getA_type()+"'");
							if(z.isNotNull(userAccountList)) {
								if(userAccountList.size()==1) {
									//获取返回的账号信息
									erp_account ea = userAccountList.get(0);
									//获取总账当前冻结数量
									BigDecimal old_frozen_bigdecimal = new BigDecimal(ea.getFrozen());
									BigDecimal new_frozen_bigdecimal = old_frozen_bigdecimal.subtract(amount_bigdecimal);
									if(new_frozen_bigdecimal.compareTo(new BigDecimal(0))>=0) {
										int updatenum = sqlSession.update("update", "update erp_account set zversion = zversion + 1 ,frozen = "+new_frozen_bigdecimal+" where zid = '"+ea.getZid()+"' and zversion = "+ea.getZversion()+" ");
										if(updatenum!=1) {
											z.Exception("更新总账冻结数据出错|更新总账条数"+updatenum);
										}
									}else {
										z.Exception("删除过程计算中出现负数，无法完成删除");
									}
								}else {
									z.Exception("明细账对应的总账出现多账户记录，执行出错");
								}
							}else {
								z.Exception("未找到明细账对应的总账");
							}
							//=============================根据出入账方向执行，出入账关联记录=============================
							if("1".equals(ad.getIn_out())) {//出账方向
								sqlSession.delete("delete", "delete from erp_account_outdetail where outid = '"+adid+"'");
							}
							
							//===============返回成功信息===============
							result.setCode(Code.SUCCESS);
							result.setMsg("删除成功");
							result.setData(adid);
						}else {
							result.setCode(Code.ERROR);
							result.setMsg("明细账的数量与冻结数据不相等，无法删除");
							result.setData(adid);
						}
					}else {
						result.setCode(Code.ERROR);
						result.setMsg("明细账的已完成解冻操作，无法删除");
						result.setData(adid);
					}
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("明细账的操作模式必须为冻结模式，否则无法删除");
					result.setData(adid);
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("根据明细账ID未找到相关记录");
				result.setData(adid);
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("明细账ID不可为空");
		}
		return result;
	}

	/**
	 * 解除冻结
	 * @param adid 明细账ID
	 * @return
	 * @throws Exception 
	 */
	public Result Unfrozen(String adid) throws Exception {
		Result result = new Result();
		if(z.isNotNull(adid)) {
			//根据明细账ID找到明细账对象
			erp_account_detail ad = sqlSession.selectOne("erp_account_detail_select_zid", adid);
			if(z.isNotNull(ad)) {
				//判读是否为冻结类记录
				if("1".equals(ad.getOperation_mode())) {
					//判读是否已完成解冻
					if(!"1".equals(ad.getIsunfrozen())) {
						//获取明细账冻结数量
						BigDecimal frozen_bigdecimal = new BigDecimal(ad.getFrozen());
						//判读冻结数量必须大于0
						if(frozen_bigdecimal.compareTo(new BigDecimal(0))>0) {
							//==========================清空明细账冻结数量==========================
							//根据出入账方面执行
							if("1".equals(ad.getIn_out())) {//出账方向
								int updatenum = sqlSession.update("update", "update erp_account_detail set zversion = zversion + 1,isunfrozen = '1',frozen = 0 where zid = '"+adid+"' and zversion = "+ad.getZversion()+"");
								if(updatenum!=1) {
									z.Exception("解除明细冻结操作时，更新明细账记录出错|更新记录条数为|"+updatenum);
								}
							}else if("0".equals(ad.getIn_out())) {//入账方向
								int updatenum = sqlSession.update("update", "update erp_account_detail set zversion = zversion + 1,isunfrozen = '1',balance = "+ad.getFrozen()+",frozen = 0 where zid = '"+adid+"' and zversion = "+ad.getZversion()+"");
								if(updatenum!=1) {
									z.Exception("解除明细冻结操作时，更新明细账记录出错|更新记录条数为|"+updatenum);
								}
							}else {
								z.Exception("非法出入账方向【"+ad.getIn_out()+"】，执行出错");
							}
							//==========================解除关联入账的冻结数量==========================
							//获取所有出账与关联的入账关联记录
							if("1".equals(ad.getIn_out())) {//出账方向
								String in_out_list_sql = "SELECT * FROM erp_account_outdetail WHERE operation_mode = '1' AND outid ='"+ad.getZid()+"' ORDER BY create_time ASC";
								List<erp_account_outdetail> in_out_list = sqlSession.selectList("erp_account_outdetail_select_sql", in_out_list_sql);
								if(z.isNotNull(in_out_list)) {
									for (erp_account_outdetail inoutdetail : in_out_list) {
										//获取关联入账冻结记录
										erp_account_detail ead = sqlSession.selectOne("erp_account_detail_select_zid", inoutdetail.getInid());
										if(z.isNotNull(ead)) {
											BigDecimal in_frozen_bigdecimal = new BigDecimal(ead.getFrozen());
											if(in_frozen_bigdecimal.compareTo(new BigDecimal(0))>0) {
												BigDecimal out_amount_bigdecimal = new BigDecimal(inoutdetail.getAmount());
												if(in_frozen_bigdecimal.compareTo(out_amount_bigdecimal)>=0) {
													//执行解除关联入账冻结数量
													int updatenum = sqlSession.update("update", "update erp_account_detail set zversion = zversion + 1 ,frozen = frozen - "+inoutdetail.getAmount()+" where zid = '"+ead.getZid()+"' and zversion = "+ead.getZversion()+"");
													if(updatenum!=1) {
														z.Exception("执行解除关联入账冻结数量出错|更新记录条数为|"+updatenum);
													}
												}else {
													z.Exception("入账冻结记录数量小于出账数量，无法执行");
												}
											}else {
												z.Exception("入账记录冻结数量为0，无法执行");
											}
										}else {
											z.Exception("未找到关联的入账冻结记录，无法执行");
										}
									}
								}else {
									z.Exception("未找到关联的入账冻结记录，无法执行");
								}
							}

							//==========================更新总账操作==========================
							UpdateAccountUN(ad.getUserid(),ad.getA_type(),ad.getIn_out(),ad.getFrozen());

							//===============修改明细账中总账可用数量与冻结数量===============
							//获取最新总账记录
							erp_account ea = sqlSession.selectOne("erp_account_select_sql","select * from erp_account where userid = '"+ad.getUserid()+"' and a_type = '"+ad.getA_type()+"'");
							if(z.isNotNull(ea)) {
								if(z.isNull(ea.getBalance())) {
									ea.setBalance("0");
								}
								if(z.isNull(ea.getFrozen())) {
									ea.setFrozen("0");
								}
								//修改明细记录
								erp_account_detail out_update_allbalance = sqlSession.selectOne("erp_account_detail_select_sql", "select * from erp_account_detail where zid = '"+adid+"'");
								if(z.isNotNull(out_update_allbalance)) {
									int updatenum = update("update erp_account_detail set all_balance = "+ea.getBalance()+" ,all_frozen = "+ea.getFrozen()+",zversion = zversion +1  where zid = '"+out_update_allbalance.getZid()+"' and zversion = "+out_update_allbalance.getZversion()+" ");
									if(updatenum!=1) {
										z.Exception("修改明细账中总账可用数量与冻结数量出错");
									}
								}else {
									z.Exception("修改明细账中总账可用数量与冻结数量出错|获取出账明细记录为空");
								}
							}else {
								z.Exception("无法获取最新总账记录");
							}

							
							//===============返回成功信息===============
							result.setCode(Code.SUCCESS);
							result.setMsg("解除冻结成功");
							result.setData(adid);
						}else {
							result.setCode(Code.ERROR);
							result.setMsg("明细账冻结数量必须大于0，否则无法执行解除冻结操作");
							result.setData(adid);
						}
					}else {
						result.setCode(Code.ERROR);
						result.setMsg("该记录已完成解除冻结操作，无法重复执行");
						result.setData(adid);
					}
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("明细账非冻结类型记录，无法执行解除冻结操作");
					result.setData(adid);
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("根据明细账ID未找到相关记录");
				result.setData(adid);
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("明细账ID不可为空");
			result.setData(adid);
		}
		return result;
	}



	/**
	 * 减少总账
	 * @param userid
	 * @param a_type
	 * @param amount
	 * @param mode
	 * @param at
	 * @throws Exception 
	 */
	private void UpdateAccountOUT(String userid, String a_type, String amount, String mode, erp_account_type at) throws Exception {
		List<erp_account> userAccountList = selectList("erp_account_select_sql","select * from erp_account where userid = '"+userid+"' and a_type = '"+a_type+"'");
		if(z.isNotNull(userAccountList)) {
			if(userAccountList.size()==1) {
				//获取返回的账号信息
				erp_account ea = userAccountList.get(0);
				if("0".equals(mode)) {//直接入账
					//计算可用数量更新值
					BigDecimal now_amount = new BigDecimal(amount);
					BigDecimal old_balance = new BigDecimal(ea.getBalance());
					BigDecimal new_balance = old_balance.subtract(now_amount);
					if(new_balance.compareTo(new BigDecimal(0))<0) {
						z.Exception("余额不足");
					}

					StringBuffer update_sql = new StringBuffer();
					update_sql.append(" update erp_account set ");
					update_sql.append(" zversion = zversion + 1 ,");
					update_sql.append(" balance = "+new_balance.toString()+" ");
					update_sql.append(" where zid='"+ea.getZid()+"' and zversion = "+ea.getZversion()+"");
					int updatenum = update(update_sql.toString());
					if(updatenum!=1) {
						z.Exception("更新用户总账信息出错|用户总账信息已变更，请重新操作");
					}

				}else if("1".equals(mode)) {//冻结入账
					//计算冻结数量更新值
					BigDecimal now_amount = new BigDecimal(amount);
					BigDecimal old_frozen = new BigDecimal(ea.getFrozen());
					BigDecimal new_frozen = old_frozen.add(now_amount);

					//计算可用数量更新值
					BigDecimal old_balance = new BigDecimal(ea.getBalance());
					BigDecimal new_balance = old_balance.subtract(now_amount);
					if(new_balance.compareTo(new BigDecimal(0))<0) {
						z.Exception("余额不足");
					}

					StringBuffer update_sql = new StringBuffer();
					update_sql.append(" update erp_account set ");
					update_sql.append(" zversion = zversion + 1 ,");
					update_sql.append(" balance = "+new_balance.toString()+", ");
					update_sql.append(" frozen = "+new_frozen.toString()+" ");
					update_sql.append(" where zid='"+ea.getZid()+"' and zversion = "+ea.getZversion()+"");
					int updatenum = update(update_sql.toString());
					if(updatenum!=1) {
						z.Exception("更新用户总账信息出错|用户总账信息已变更，请重新操作");
					}
				}else {
					z.Exception("非法入账操作模式|"+mode);
				}
			}else {
				z.Exception("客户出现多账户记录，执行出错");
			}
		}else {
			z.Exception("未找到客户对应总账");
		}

	}

	/**
	 * 增加总账
	 * @param userid
	 * @param a_type
	 * @param amount
	 * @param mode
	 * @param at
	 * @throws Exception
	 */
	private void UpdateAccountIN(String userid,String a_type,String amount,String mode,erp_account_type at) throws Exception {
		List<erp_account> userAccountList = selectList("erp_account_select_sql","select * from erp_account where userid = '"+userid+"' and a_type = '"+a_type+"'");
		if(z.isNotNull(userAccountList) && userAccountList.size()==1) {//用户账户已创建，更新可以数量
			//获取返回的账号信息
			erp_account ea = userAccountList.get(0);
			if("0".equals(mode)) {//直接入账
				//计算可用数量更新值
				BigDecimal now_amount = new BigDecimal(amount);
				BigDecimal old_balance = new BigDecimal(ea.getBalance());
				BigDecimal new_balance = old_balance.add(now_amount);

				StringBuffer update_sql = new StringBuffer();
				update_sql.append(" update erp_account set ");
				update_sql.append(" zversion = zversion + 1 ,");
				update_sql.append(" balance = "+new_balance.toString()+" ");
				update_sql.append(" where zid='"+ea.getZid()+"' and zversion = "+ea.getZversion()+"");
				int updatenum = update(update_sql.toString());
				if(updatenum!=1) {
					z.Exception("更新用户总账信息出错|用户总账信息已变更，请重新操作");
				}

			}else if("1".equals(mode)) {//冻结入账
				//计算可用数量更新值
				BigDecimal now_amount = new BigDecimal(amount);
				BigDecimal old_frozen = new BigDecimal(ea.getFrozen());
				BigDecimal new_frozen = old_frozen.add(now_amount);

				StringBuffer update_sql = new StringBuffer();
				update_sql.append(" update erp_account set ");
				update_sql.append(" zversion = zversion + 1 ,");
				update_sql.append(" frozen = "+new_frozen.toString()+" ");
				update_sql.append(" where zid='"+ea.getZid()+"' and zversion = "+ea.getZversion()+"");
				int updatenum = update(update_sql.toString());
				if(updatenum!=1) {
					z.Exception("更新用户总账信息出错|用户总账信息已变更，请重新操作");
				}
			}else {
				z.Exception("非法入账操作模式|"+mode);
			}

		}else if(z.isNotNull(userAccountList) && userAccountList.size()==0) {//用户账户未创建
			//创建用户账户
			erp_account a = new erp_account();
			a.setZid(z.newZid("erp_account"));
			a.setUserid(userid);//用户ID
			a.setA_type(a_type);//账户类型
			a.setUnit(at.getUnit());//单位
			if("0".equals(mode)) {//直接入账
				BigDecimal new_balance = new BigDecimal(amount);
				a.setBalance(new_balance.toString());//可用数量
				a.setFrozen("0");//冻结数量
			}else if("1".equals(mode)) {//冻结入账
				BigDecimal new_frozen = new BigDecimal(amount);
				a.setBalance("0");//可用数量
				a.setFrozen(new_frozen.toString());//冻结数量
			}else {
				z.Exception("非法入账操作模式|"+mode);
			}
			int insertnum = insert("erp_account_insert", a);
			if(insertnum!=1) {
				z.Exception("创建用户账户出错|未成功创建");
			}
		}else {
			//出现多长账户记录
			z.Exception("客户出现多账户记录，执行出错");
		}
	}
	/**
	 * 解除冻结--更新总账记录
	 * @param userid
	 * @param a_type
	 * @param in_out
	 * @param amount
	 * @throws Exception
	 */
	private void UpdateAccountUN(String userid, String a_type, String in_out, String amount) throws Exception {
		if(z.isNotNull(userid)) {
			if(z.isNotNull(a_type)) {
				if(z.isNotNull(in_out)) {
					List<erp_account> userAccountList = selectList("erp_account_select_sql","select * from erp_account where userid = '"+userid+"' and a_type = '"+a_type+"'");
					if(z.isNotNull(userAccountList)) {
						if(userAccountList.size()==1) {
							//获取总账
							erp_account ea = userAccountList.get(0);
							//根据出入账方面执行
							if("1".equals(in_out)) {//出账方向
								//总账冻结数量减掉本次解冻数量
								int updatenum = sqlSession.update("update", "update erp_account set zversion = zversion + 1 ,frozen = frozen - "+amount+" where zid = '"+ea.getZid()+"' and zversion = "+ea.getZversion()+"");
								if(updatenum!=1) {
									z.Exception("解除明细冻结操作时，更新总账记录出错|更新记录条数为|"+updatenum);
								}
							}else if("0".equals(in_out)) {//入账方向
								//总账冻结数量减掉本次解冻数量，并且总账可用数量加上本次解冻数量
								int updatenum = sqlSession.update("update", "update erp_account set zversion = zversion + 1 ,frozen = frozen - "+amount+",balance = balance + "+amount+" where zid = '"+ea.getZid()+"' and zversion = "+ea.getZversion()+"");
								if(updatenum!=1) {
									z.Exception("解除明细冻结操作时，更新总账记录出错|更新记录条数为|"+updatenum);
								}
							}else {
								z.Exception("非法出入账方向【"+in_out+"】，执行出错");
							}
						}else {
							z.Exception("明细账客户出现多总账记录，执行出错");
						}
					}else {
						z.Exception("未找到明细账对应总账");
					}
				}else {
					z.Exception("明细账账户出入账方向为空，无法执行解除冻结操作");
				}
			}else {
				z.Exception("明细账客户账户类型为空，无法执行解除冻结操作");
			}
		}else {
			z.Exception("明细账客户ID为空，无法执行解除冻结操作");
		}
	}

	/**
	 * 查询方法
	 * @param bean
	 * @param string
	 * @return
	 * @throws Exception 
	 */
	public ModelAndView select(HashMap<String,String> bean,String viewName) throws Exception {
		ModelAndView model = new ModelAndView(viewName);
		if(!z.isNotNull(bean.get("pagenum")) || !z.isNotNull(bean.get("rowcount"))) {
			bean.put("pagenum", "1");
			bean.put("rowcount", z.sp.get("rowcount"));
		}
		
		//获取列表，并执行格式化
		List<HashMap<String,String>> list = selectList(bean);
		for (HashMap<String, String> map : list) {
			String str_a_type = map.get("a_type");
			if(z.isNotNull(str_a_type)) {
				//获取账户类型对象
				erp_account_type at = z.account_type.get(str_a_type);
				if(z.isNotNull(at)) {
					//格式化可用数量
					String str_balance =String.valueOf(map.get("balance"));
					if(z.isNotNull(str_balance)) {
						//格式化数字
						if(z.isNotNull(at.getDecimals()) && StringUtil.isBigDecimal(str_balance)) {
							map.put("balance", MathUtil.FormatNumber(str_balance, new Integer(at.getDecimals())));
						}
					}
					
					//格式化冻结数量
					String str_frozen =String.valueOf(map.get("frozen"));
					if(z.isNotNull(str_frozen)) {
						//格式化数字
						if(z.isNotNull(at.getDecimals()) && StringUtil.isBigDecimal(str_frozen)) {
							map.put("frozen", MathUtil.FormatNumber(str_frozen, new Integer(at.getDecimals())));
						}
					}
				}
			}
		}
		model.addObject("list", list);
		bean.put("queryinfolist", getQueryInfoList(bean));//常用用户该表单查询条件
		model.addObject("bean", bean);
		return model;
	}

}
